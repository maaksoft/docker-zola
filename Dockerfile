FROM bitnami/minideb
ENV ZOLA_VERSION v0.9.0

RUN install_packages curl ca-certificates && \
    curl -L https://github.com/getzola/zola/releases/download/$ZOLA_VERSION/zola-$ZOLA_VERSION-x86_64-unknown-linux-gnu.tar.gz | tar xz -C /usr/bin && \
    zola --version
